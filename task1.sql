-- Task 1: BRANCH
CREATE TABLE branch(
  branch_id INT PRIMARY KEY NOT NULL AUTO_INCREMENT,
  branch_addr VARCHAR(255),
  ISBN VARCHAR(50) NOT NULL,
  tittle VARCHAR (100),
  author VARCHAR (100),
  publisher VARCHAR (100),
  num_copies INT DEFAULT 0
);