-- Task 4 :DOCTOR
CREATE TABLE doctor(
  doctor_id int PRIMARY KEY NOT NULL,
  doctor_name VARCHAR(100),
  secretary_name VARCHAR(100),
  patient_id INT,
  prescription_id INT,
  FOREIGN KEY (patient_id) REFERENCES patient(patient_id),
  FOREIGN KEY (prescription_id) REFERENCES prescription(prescription_id)
);
CREATE TABLE patient(
  patient_id int PRIMARY KEY NOT NULL,
  patient_name VARCHAR (100),
  patient_dob DATE,
  patient_address VARCHAR (255)
);
CREATE table prescription(
  prescription_id int PRIMARY KEY NOT NULL,
  drug VARCHAR(100),
  date_of_visit DATE,
  dosage VARCHAR (100)
);