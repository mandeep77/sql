
-- Task 3: PATIENT
CREATE TABLE patient(
  patient_id INT PRIMARY KEY NOT NULL,
  patient_name VARCHAR(100),
  dob DATE,
  Patient_address VARCHAR(255),
  prescription_id INT,
  FOREIGN KEY (prescription_id) REFERENCES prescription(prescription_id)
);
CREATE TABLE prescription(
  prescription_id INT PRIMARY KEY NOT NULL,
  drug VARCHAR(100),
  date_of_visit DATE,
  dosage VARCHAR(100),
  doctor VARCHAR(100),
  secretary VARCHAR(100)
);