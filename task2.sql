
-- Task 2: CLIENT
CREATE TABLE client(
  client_id INT PRIMARY KEY AUTO_INCREMENT NOT NULL,
  name_client VARCHAR(100),
  location_client VARCHAR (255),
  manager_id INT,
  contract_id INT,
  staff_id INT,
  FOREIGN KEY (manager_id) REFERENCES manager(manager_id),
  FOREIGN KEY (contract_id) REFERENCES contract_(contract_id),
  FOREIGN key (staff_id) REFERENCES staff(staff_id)
);
CREATE TABLE manager(
  manager_id INT NOT NULL PRIMARY KEY,
  managar_name VARCHAR (100),
  manager_location VARCHAR (255)
);
CREATE TABLE contract_(
  contract_id int NOT NULL PRIMARY KEY,
  estimated_cost int,
  completion_date DATE
);
CREATE TABLE staff(
  staff_id int NOT NULL PRIMARY KEY,
  staff_name VARCHAR(100),
  staff_location VARCHAR (255)
);